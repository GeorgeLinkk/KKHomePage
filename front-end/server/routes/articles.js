const Router = require('koa-router');
const  articles = require('../controllers/articles');  //用户相关接口逻辑
let fs = require('fs');
const router = new Router();

router.post('/articles/getArticleList', articles.getArticleList)
module.exports = router