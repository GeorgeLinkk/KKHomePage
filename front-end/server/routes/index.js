
const Router = require('koa-router');
const  auth = require('../controllers/auth');  //用户相关接口逻辑
let fs = require('fs');
const router = new Router();

router
    .post('/auth/loginIn', auth.loginIn)
    .get('/getJson', async ctx => {
        // 后端允许cors跨域请求
        // // 返回给前端的数据
        ctx.response.body = JSON.parse(fs.readFileSync( './static/data.json'));
    });
    

module.exports = router