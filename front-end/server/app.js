const Koa = require('koa')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const router = require('./routes/index');
const articlesRoute = require('./routes/articles');
const bodyParser = require('koa-bodyparser');
const parameter = require('koa-parameter');

let myResponse = require('./minddleware/response')

const app = new Koa()
//解析请求体
app.use(bodyParser());
//统一response格式
app.use(myResponse.routerResponse());
//参数校验
app.use(parameter(app))
app.use(articlesRoute.routes()).use(router.allowedMethods());
app.use(router.routes()).use(router.allowedMethods())
// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = app.env !== 'production'

async function start () {
  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  const {
    host = process.env.HOST || '127.0.0.1',
    port = process.env.PORT || 3000
  } = nuxt.options.server

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  app.use((ctx) => {
    ctx.status = 200
    ctx.respond = false // Bypass Koa's built-in response handling
    ctx.req.ctx = ctx // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
    nuxt.render(ctx.req, ctx.res)
  })

  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}

start()
