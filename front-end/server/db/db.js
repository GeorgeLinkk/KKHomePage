const mongoose = require('mongoose');
// 指定用户的数据库
const dbUserName = 'George';
const dbUserPwd = '123456';
const dbIP = '120.26.63.184:27017'
const dbName = 'nuxthomepage'
// mongo 连接地址 
const dbs = 'mongodb://' + dbUserName + ':' + dbUserPwd +'@' + dbIP + '/' + dbName
// const dbs = 'mongodb://@localhost:27017/?authSource=kkhomepage&readPreference=primary'
console.log(dbs)
mongoose.Promise = global.Promise
mongoose.connect(dbs, {
  useMongoClient:true,
  // useNewUrlParser: true,
  // auto_reconnect: true
});
mongoose.connection.on('connected', function () {
  console.log('connected');
});
mongoose.connection.on('error', function (err) {
  console.log(' error: ' + err);
});
mongoose.connection.on('disconnected', function () {
  console.log(' disconnected');
});

module.exports = mongoose;