const mongoose = require('../db')
const Schema = mongoose.Schema
const BudgetsSchema = new Schema({
    title:{                 //标题
        type:String,
        required:true
    },
    date:{                  //日期
        type:Date,
        require:true
    },
    amount:{                //金额
        type:Number,
        require:true
    },
    userId: {               //提交人
        type: String,
        required:true
    },
    tip:{                   //备注
        type:String
    },
    tags:{                  //支出/收入类型
        type:Array
    },
    amountType:{            //1：支出 2：收入
        type:Number,
        required:true
    }
})
module.exports = mongoose.model('Budgets', BudgetsSchema, 'Budgets')