const mongoose = require('../db')
const Schema = mongoose.Schema
const ArticlesSchema = new Schema({
    title:{                 //标题
        type:String,
        required:true
    },
    publicDate:{                  //日期
        type:Date,
        require:true
    },
    modifyDate:{                  //日期
        type:Date,
        require:true
    },
    content:{
        type:String,
        required:true
    },
    views:Number,
    isRecommended:Boolean,
    isOriginal:Boolean,
    tags:Array,
    thumb:String
})
module.exports = mongoose.model('Articles', ArticlesSchema, 'Articles')