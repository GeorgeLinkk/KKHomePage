const Users = require('./models/users')
const Budgets = require('./models/budgets')
const Tags = require('./models/tags')
const Articles = require('./models/articles')

module.exports = {
    Users:Users,
    Budgets:Budgets,
    Tags:Tags,
    Articles:Articles
}