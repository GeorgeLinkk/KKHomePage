const Articles = require('../db/index').Articles

//获取总览数据
let getArticleList = async (ctx) => {
    let start = ctx.request.start?ctx.request.start:0;
    let limit = ctx.request.limit?ctx.request.limit:20;
    const list = await Articles.find().skip(start).limit(parseInt(limit)).exec(function (err, data) {
        if(err) {
            ctx.fail('服务器繁忙，请稍后重试！',500)
        }
    })
    let data = {
        list:list,
        totalCount:5
    }
    ctx.success(data);
}
let getArticleById = async (ctx)=>{
    let { id } = ctx.request.id;
    let detail = await Articles.find({'_id':id});
    ctx.response.body = {
        status:200,
        data:{
            list:list,
            totalCount:5
        }
    }
}

const articlesFunc = {
    getArticleList,
    getArticleById
};
module.exports = articlesFunc