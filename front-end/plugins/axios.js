export default function({ app: { $axios } }) {
    $axios.defaults.baseURL = 'http://localhost:3000/'
    $axios.interceptors.request.use(config => {
      return config
    })
}