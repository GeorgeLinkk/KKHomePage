const formatDateTime = (date)=>{
    console.log(1)
    let newDate;
    if(date){
        newDate = new Date(date);  
    }else{
        newDate = new Date();  
    }
    let year = newDate.getFullYear();  
    let month = newDate.getMonth()+1;
    let day = newDate.getDate(); 
    let hour = newDate.getHours();
    let minute = newDate.getMinutes();
    let second = newDate.getSeconds();
    const formatZero = (count)=>{
        return (count&&count.toString().length<2)?('0' + count):count
    }
    month = formatZero(month);
    day = formatZero(day);
    hour = formatZero(hour);
    minute = formatZero(minute);
    second = formatZero(second);
    // if(day&&day.toString().length<2){
    //     day = '0' + day;
    // } 
    let myDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    console.log(myDate)
    return myDate;
}
const formatDate = (date)=>{
    let newDate;
    if(date){
        newDate = new Date(date);  
    }else{
        newDate = new Date();  
    }
    let year = newDate.getFullYear();  
    let month = newDate.getMonth()+1;
    let day = newDate.getDate(); 
    const formatZero = (count)=>{
        return (count&&count.toString().length<2)?('0' + count):count
    }
    month = formatZero(month);
    day = formatZero(day);
    let myDate = year + '-' + month + '-' + day;
    console.log(myDate)
    return myDate;
}
export default{
    formatDateTime:formatDateTime,
    formatDate:formatDate
}