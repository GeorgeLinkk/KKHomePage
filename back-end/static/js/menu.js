export default {
    menuData: [
        {
            icon: 'kkIcon-home',
            id:'index',
            title : '首页',
            menuRoute: 'Homepage' 
        },
        {
            icon: 'kkIcon-sort',
            id:'3',
            title : '我的组件',
            list: [
                {
                    id:'31',
                    title : 'kk-icon',
                    menuRoute: 'KKIcon' 
                }
            ]
        },
        {
            icon: 'kkIcon-earth',
            id:'4',
            title : '一些小玩意',
            list: [
                {
                    id:'41',
                    title : '跳一跳',
                    menuRoute: 'Jumper' 
                },
                {
                  id:'42',
                  title : 'VR看房',
                  menuRoute: 'VRroom' 
                }
            ]
        },
        {
          icon: 'kkIcon-earth',
          id:'5',
          title : '文章管理',
          list: [
              {
                  id:'51',
                  title : '文章列表',
                  menuRoute: 'ArticleList' 
              }
          ]
        }
    ]
}