/**
 * 参数配置值 @see field_required：表单字段必选中（展示）,field_default：表单默认字段，
 * @see name：名称,type:类型,width：长度,ext:扩展规则,overflow:是否超出隐藏
 */
 const data = {
  // 渠道列表
  '1': [
    {
      name: 'id',
      key: '_id',
      width: 250,
    },
    {
      name: '标题',
      key: 'title'
    },
    {
      name: '创建人',
      key: 'author'
    },
    {
      name: '编辑人',
      key: 'updater'
    },
    {
      name: '创建日期',
      key: 'createDate',
      type: 'date',
      width: 200,
      ext: {
        formatter: 'YYYY-MM-DD HH:mm:ss'
      }
    },
    {
      name: '编辑时间',
      key: 'modifyDate',
      type: 'date',
      width: 200,
      ext: {
        formatter: 'YYYY-MM-DD HH:mm:ss'
      }
    }
  ]
}

export default data
