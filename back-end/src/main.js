// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '../static/css/style.css';
import '../static/iconfont/iconfont.css';
import 'element-ui/lib/theme-chalk/index.css';

import Vue from 'vue'
import {router} from './router'
import elementui from 'element-ui'
import store from './store'

Vue.config.productionTip = false
Vue.use(elementui)
// axios.defaults.withCredentials = true;
// axios.defaults.timeout = 600000;
// axios.defaults.headers.post['Content-Type'] = 'application/json';


new Vue({
  router,
  store
}).$mount('#app')
