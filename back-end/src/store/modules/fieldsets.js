// import storeage from 'store'
import FIELD_SET from '@/directory/fieldset'
export default {
  namespaced: true,
  state: {
    fieldset: FIELD_SET
  },
  mutations: {},
  actions: {
    getField({ state }, fieldsetId) {
      return Promise.resolve(FIELD_SET[fieldsetId])
    }
  }
}
