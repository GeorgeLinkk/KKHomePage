export default {
  data() {
    return { maxTableHeight: 0 }
  },

  mounted() {
    setTimeout(this._doResize)
    window.addEventListener('resize', this._doResize)
  },
  activated() {
    window.addEventListener('resize', this._doResize)
  },
  deactivated() {
    window.removeEventListener('resize', this._doResize)
  },
  methods: {
    _doResize(e) {
      setTimeout(() => {
        const mainEl = document.querySelector('.layoutContent')
        const container = mainEl.children[0]
        if (!container) return
        if (container.scrollHeight > container.clientHeight) {
          const overflowHeight = container.scrollHeight - container.clientHeight
          this.maxTableHeight = this.maxTableHeight - overflowHeight
          return
        } else {
          this.maxTableHeight = 0
          this.$nextTick(() => {
            const blankEl = document.createElement('div')
            blankEl.style.height = container.clientHeight + 'px'
            container.appendChild(blankEl)
            const overflowHeight = container.scrollHeight - container.clientHeight
            container.removeChild(blankEl)
            if (overflowHeight > 0) {
              this.maxTableHeight = container.clientHeight - overflowHeight
            }
          })
        }
      })
    }
  },
  beforeDestroy() {
    window.removeEventListener('resize', this._doResize)
  },
}
