import { asyncComponent } from '@/lib/utils';

const Jumper = asyncComponent('collections/jumper/jumper');
const VRroom = asyncComponent('collections/vrroom/vrroom');

export let r = [
  {
    path:'/collections/jumper',
    name:'Jumper',
    meta:{
      title:'跳一跳',
      elIcon:'el-icon-medal'
    },
    component:Jumper
  },
  {
    path:'/collections/vrroom',
    name:'VRroom',
    meta:{
      title:'VR看房',
      elIcon:'el-icon-office-building'
    },
    component:VRroom
  }
]