import { asyncComponent } from '@/lib/utils';

const KKBudget = asyncComponent('budget/KKBudget');
const CCBudget = asyncComponent('budget/CCBudget');
const AllBudget = asyncComponent('budget/AllBudget');

export let r = [
    //收支情况
    {
        path: '/budget/kk-budget',
        name: 'KKBudget',
        component: KKBudget
    },
    {
        path: '/budget/cc-budget',
        name: 'CCBudget',
        component: CCBudget
    },
    {
        path: '/budget/all-budget',
        name: 'AllBudget',
        component: AllBudget
    },
]