import { asyncComponent } from '@/lib/utils';

const BacklogPlan = asyncComponent('plan/BacklogPlan');
const MyPlan = asyncComponent('plan/MyPlan');
const FinishedPlan = asyncComponent('plan/FinishedPlan');

export let r = [
    //计划书
    {
        path: '/plan/backlog-plan',
        name: 'BacklogPlan',
        component: BacklogPlan
    },
    {
        path: '/plan/my-plan',
        name: 'MyPlan',
        component: MyPlan
    },
    {
        path: '/plan/finished-plan',
        name: 'FinishedPlan',
        component: FinishedPlan
    },
]