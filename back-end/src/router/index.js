import Vue from 'vue'
import Router from 'vue-router'

import { asyncComponent } from '@/lib/utils';
import store from '../store'
const Home = resolve => require(['@/App'], resolve)
const Login = resolve => require([`@/layout/Login`], resolve)
const NotFound = resolve =>require([`@/layout/NotFound`],resolve)



Vue.use(Router)

// 手动输入地址跳转的页面白名单
const whiteList = [
  '/'
];
// 默认不需要权限的页面
const constantRouter = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]
// 404页面，放在动态路由后面添加，避免出现动态加载全为404
const notFoundRouter = [
  {
    path: '/error',
    component: Home,
    redirect:'/error/notFound',
    children: [
      {
        path: '/error/notFound',
        name: 'NotFound',
        component: NotFound
      }
    ]
  },
]

export const router = new Router({
  routes: constantRouter
});
// 需要权限的路由（用来匹配不同角色权限得到用户最终路由）
export const asyncRouter = [
  {
    path: '/',
    component: Home,
    redirect:'/homepage',
    meta:{
      title:'主页',
      elIcon:'el-icon-house'
    },
    children: registerRouter(require.context('./', false, /homepage.route.js$/))
  },
  {
    path: '/budget',
    component: Home,
    redirect:'/budget/all-budget',
    children: registerRouter(require.context('./', false, /budget.route.js$/))
  },
  {
    path: '/component',
    component: Home,
    alwaysShow:true,
    meta:{
      title:'个人组件',
      elIcon:'el-icon-copy-document'
    },
    redirect:'/component/Icon',
    children: registerRouter(require.context('./', false, /component.route.js$/))
  },
  {
    path: '/collections',
    component: Home,
    alwaysShow:true,
    meta:{
      title:'个人收藏',
      elIcon:'el-icon-collection'
    },
    redirect:'/collections/jumper',
    children: registerRouter(require.context('./', false, /collections.route.js$/))
  },
  {
    path: '/article',
    component: Home,
    alwaysShow:true,
    meta:{
      title:'文章管理',
      elIcon:'el-icon-notebook-2'
    },
    redirect:'/article/ArticleList',
    children: registerRouter(require.context('./', false, /article.route.js$/))
  }
]
// 批量引入路由
function registerRouter(requireContext) {
  let rMap = [];
  requireContext.keys().forEach(comp => {
      let vueComp = requireContext(comp);
      let rList = vueComp.r || [];
      for(let i =0,l=rList.length;i<l;i++){
          rMap.push(rList[i]);
      }
  });
  return rMap;
}
/*  根据接口返回的路由权限匹配最终路由
**  userRouter 接口返回的用户路由权限
**  asyncRouter 本地的权限路由表
*/
function matchRouter(userRouter,asyncRouter){
  console.log(userRouter,asyncRouter)
  return new Promise((resolve)=>{
    function createRouter(asyncRouter){
      return asyncRouter.filter(item => {
        return userRouter.some(r => r.path == item.path)
      }).map(m => {
          m = Object.assign({}, m)
          if (m.children) {
              m.children = createRouter(m.children)
          }
          return m
      })
    }
    const routes = createRouter(asyncRouter);
    resolve(routes)
  })
}

// 路由拦截，无权限则重定向到homepage
router.beforeEach((to, form, next) => {
  console.log(1)
  if (store.getters.GET_USERDATA.token) {
    console.log(2)
    if (store.getters.GET_USERROUTERS.length !== 0) {
      console.log(3,to)
      if (to.matched.length) {
        next()
      } else {
        console.log(5)
        router.replace('/error/notFound')
      }
    } else {
      console.log(6)
        //如果没有权限列表，将重新向后台请求一次
      store.dispatch('getUserRouters').then(() => {
        //调用权限匹配的方法
        matchRouter(store.getters.GET_USERROUTERS, asyncRouter).then(res => {
            //将匹配出来的权限列表进行addRoutes
            console.log(res)
            store.dispatch('setMenu',res)
            router.addRoutes(res);
            router.addRoutes(notFoundRouter)
          next({ ...to, replace: true })
        })
      }).catch(() => {
        console.log(7)
        router.replace('/')
      })
    }
  } else {
    console.log(8)
    if (whiteList.indexOf(to.path) >= 0) {
      console.log(9)
      next()
    } else {
      console.log(10)
      if (to.path === '/login') {
        next();
      } else {
        router.replace('/login')
      }
    }
  }
});