import { asyncComponent } from '@/lib/utils';
const Home = resolve => require(['@/App'], resolve)
const ArticleList = asyncComponent('article/ArticleList');
const EditArticle = asyncComponent('article/EditArticle');
export let r = [
  //文章列表
  {
    path: '/article/ArticleList',
    name: 'ArticleList',
    meta:{
      title:'文章列表',
      elIcon:'el-icon-tickets'
    },
    component: ArticleList,
  },
  //添加/编辑文章
  {
    path: '/article/EditArticle',
    name: 'EditArticle',
    hidden: true,
    meta:{
      title:'文章编辑',
      elIcon:'el-icon-edit'
    },
    component: EditArticle
  }

]