import { asyncComponent } from '@/lib/utils';

const KKIcon = asyncComponent('component/Icon');

export let r = [
    {
        path:'/component/Icon',
        name:'KKIcon',
        meta:{
          title:'图标',
          elIcon:'el-icon-collection-tag'
        },
        component:KKIcon
    },
]