import { asyncComponent } from '@/lib/utils';

const HomePage = asyncComponent('HomePage');

export let r = [
    //首页
    {
        path: '/homepage',
        name: 'Homepage',
        component: HomePage
    },
]