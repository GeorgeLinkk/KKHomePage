import RenderColumns from './index'
import { Table, TableColumn } from 'element-ui'
import { mount } from '@vue/test-utils'
describe(`RenderColumns Start`, () => {
  it(`Simple Column Config`, () => {
    const config = [
      {
        name: '申请时间',
        key: 'success_time',
        fixed: true,
        type: 'date',
        ext: {
          format: 'YYYY-MM-DD hh:mm:ss'
        }
      },
      { name: '报表名称', key: 'ark_name' },
      { name: '生成名称', key: 'report_name' },
      { name: '生成状态', key: 'status_str' },
      { name: '生成者', key: 'admin_name' },
      {
        fixed: 'right',
        name: '操作',
        renderContent(h, scope) {
          return <div>下载</div>
        }
      }
    ]
    const Columns = {
      functional: true,
      render(h) {
        return h(RenderColumns, {
          props: {
            columnConfigs: config
          }
        })
      }
    }
    const wrapper = mount(Table, {
      sync: false,
      propsData: {
        data: [{ ark_name: '报表1' }]
      },
      slots: {
        default: Columns
      }
    })
    const _columns = wrapper.findAllComponents(TableColumn)
    expect(_columns).toHaveLength(6)
  })

  it(`Multi Level Thead`, () => {
    const config = [
      { key: '1', name: '1', child: [{ key: '1-1', name: '1-1' }] },
      { key: '2', name: '2', child: [{ key: '2-1', name: '2-2' }] },
      { key: '3', name: '3' }
    ]
    const Columns = {
      sync: false,
      functional: true,
      render(h) {
        return h(RenderColumns, {
          props: {
            columnConfigs: config
          }
        })
      }
    }
    const wrapper = mount(Table, {
      slots: {
        default: [Columns]
      }
    })
    const _columns = wrapper.findAllComponents(TableColumn)
    expect(_columns).toHaveLength(5)
  })
})
