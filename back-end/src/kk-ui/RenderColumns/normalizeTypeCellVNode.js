import moment from 'moment'
import { Image } from 'element-ui'
// import { valueToLabel } from '@/constants/directory/index'
// import ApproveColumn from './components/ApproveColumn'
// import store from '@/store'
// import { ImplictContact } from '@/components/ImplictContact/index'
/**
 * @param {Function} h     Vue.createElement
 * @param {Object} config  配置项
 * @param {Object} scope   {row,column}
 * 通过config的类型，生成对应的VNode。
 * @return {VNode}
 */
// TODO:可以增加字段的类型,通过类型，这里可以预先处理好VNode
const normalizeTypeCellVNode = (h, config, { row }) => {
  let contentVNode = null
  const value = row[config.key]
  switch (config.type) {
    // 时间
    case 'date': {
      const { ext } = config
      const defaultFormatter = 'YYYY-MM-DD'
      const formatter = ext.formatter || defaultFormatter
      if (!value) {
        return <span>-</span>
      }
      const formatedDate = moment(new Date(value).getTime()).format(formatter)
      if (config.dateBetween) {
        // 时间范围类型
        const otherFormatter = ext.otherFormatter || defaultFormatter
        const otherValue = row[ext.otherKey]
        const otherDate = moment(otherValue).format(otherFormatter)
        contentVNode = (
          <span>
            {formatedDate} - {otherDate}
          </span>
        )
      } else {
        contentVNode = <span>{formatedDate}</span>
      }

      break
    }
    // 图片
    case 'image': {
      contentVNode = (
        <Image
          src={value}
          preview-src-list={[value]}
          lazy
          style="width:25px;height:45px;"
        />
      )
      break
    }
    // // 常量字典
    // case 'dict': {
    //   const { ext } = config
    //   const { dictKey } = ext || {}
    //   const label = valueToLabel(value, dictKey)
    //   contentVNode = <span>{label}</span>
    //   break
    // }
    // // 业务字典
    // case 'bizdict': {
    //   const { ext } = config
    //   const { dictKey } = ext || {}
    //   const label = store.getters['bizdict'](value, dictKey)
    //   contentVNode = <span>{label}</span>
    //   return contentVNode
    // }
    // // 审批节点
    // case 'approve': {
    //   contentVNode = <ApproveColumn config={value} />
    //   break
    // }
    // // 手机号
    // case 'mobile': {
    //   contentVNode = <ImplictContact custId={row.id}>{value}</ImplictContact>
    //   break
    // }
    // 默认都是text
    default: {
      contentVNode = <span>{value}</span>
    }
  }
  return contentVNode
}

export default normalizeTypeCellVNode
