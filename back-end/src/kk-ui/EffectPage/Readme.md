### EffectPage Example

```js static
const request = () => new Promise()
const fieldsetId = '1'
```

- 获取数据以及字段的配置

```jsx static
<EffectPage :fieldsetId="fieldsetId" :request="request">
  <template v-slot:content="{data,loading,fields}">
    <YourComponents :data="data" :fields="fields" v-loading="loading" />
  </template>
</EffectPage>
```
