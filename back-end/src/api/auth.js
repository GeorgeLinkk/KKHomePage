import INDEX from '@/api/index'
import request from '@/lib/request'
const API_URL = INDEX.API_URL;

export default {
    API_URL:API_URL,
    login: function (params) {
        return request({
            url: API_URL + '/auth/loginIn',
            method: 'POST',
            data: params
        });
    },
    logout: function () {
        return request({
            url: API_URL + '/auth/logout',
            method: 'POST'
        });
    }
}