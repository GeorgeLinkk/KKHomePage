import INDEX from '@/api/index'
import request from '@/lib/request'
const API_URL = INDEX.API_URL;

export default {
    API_URL:API_URL,
    getRencentBudget: function (params) {
        return request({
            url: API_URL + '/homepage/getRencentBudget',
            method: 'POST',
            data: params
        });
    }
}