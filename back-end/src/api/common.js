import INDEX from '@/api/index'
import request from '@/lib/request'
const API_URL = INDEX.API_URL;

export default {
    API_URL:API_URL,
    getTableDdisplayField:params => {
      return request({
        url: API_URL + `/common/get_table_display_field`,
        method: 'get',
        params
      })
    }
}