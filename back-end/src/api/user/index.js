import INDEX from '@/api/index'
import request from '@/lib/request'
const API_URL = INDEX.API_URL;

export default {
    API_URL:API_URL,
    getUserInfo: function (params) {
        return request({
            url: API_URL + '/user/getUserInfo',
            method: 'GET',
            params: params
        });
    },
    getUserRouters: function (params){
      return request({
        url: API_URL + '/user/getUserRouters',
        method: 'GET',
        params: params
    });
    }
}