import INDEX from '@/api/index'
import request from '@/lib/request'
const API_URL = INDEX.API_URL;

export default {
    API_URL:API_URL,
    getArticleList: function (params) {
        return request({
            url: API_URL + '/article/getArticleList',
            method: 'GET',
            params: params
        });
    },
    addArticle: function (params) {
        return request({
            url: API_URL + '/article/addArticle',
            method: 'POST',
            data: params
        });
    }
}