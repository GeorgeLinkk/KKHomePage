'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
const API_URL = "'http://localhost:3000'"   //开发环境
// const API_URL = "'http://www.linkk.top:3000'"    //生产环境

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API:API_URL
})
