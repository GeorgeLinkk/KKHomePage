// Karma configuration
// Generated on Tue Dec 15 2020 14:52:14 GMT+0800 (GMT+08:00)
var webpackConfig = require('./build/webpack.test.conf')

module.exports = function(config) {
  config.set({
    webpack:webpackConfig,
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha','sinon-chai'],


    // list of files / patterns to load in the browser
    files: [
      'test/unit/**/*Spec.js'
    ],


    // list of files / patterns to exclude
    exclude: [
    ],

    //预处理器--增加webpack、sourcemap、coverage
    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'test/unit/**/*.specs.js': ['webpack', 'sourcemap', 'coverage']
    },

    // 新增插件
    plugins: [
      'karma-webpack',
      'karma-sourcemap-loader',
      'karma-mocha',
      'karma-chai',
      'karma-sinon-chai',
      'karma-firefox-launcher',
      'karma-chrome-launcher',
      'karma-spec-reporter',
      'karma-coverage'
    ],
    // 新增spec、coverage
    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec','coverage','progress'],

    // 新增覆盖率报告
    coverageReporter: {
      // dir: './coverage',
      dir: './test/unit/coverage',
      reporters: [
        { type: 'html', subdir: 'report-html' },
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome', 'Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
