const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const PurifyCSS = require('purifycss-webpack')
const glob = require('glob-all')

module.exports = {
  mode: "development",
  //mode:productioin,     //生产模式自动压缩代码
  entry: ["./src/index.ts"],
  output: {
    path: path.join(__dirname, "dist"),
    filename: "bundle.js"
  },
  devtool:"cheap-module-eval-source-map",// 开发环境配置
  // devtool:"cheap-module-source-map",   // 线上生成配置
  module: {
    loaders:[
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "postcss-loader",
            "sass-loader"
          ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)/,
        use: {
          loader: 'url-loader',
          options: {
            outputPath: 'images/', // 图片输出的路径
            limit: 10 * 1024
          }
        }
      },
      {
        test: /\.(eot|woff2?|ttf|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name]-[hash:5].min.[ext]',
              limit: 5000,
              publicPath: 'fonts/',
              outputPath: 'fonts/'
            }
          }
        ]
      },
      {
        test: /\.ts(x?)$/,
        use: {
          loader: 'awesome-typescript-loader',
          options: {
            
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html' // 最终创建的文件名
    }),
    new MiniCssExtractPlugin({ //抽取css文件,与js并行下载，提高页面加载效率
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new PurifyCSS({
      paths: glob.sync([
        // 要做 CSS Tree Shaking 的路径文件
        path.resolve(__dirname, './src/*.html'), // 同样需要对 html 文件进行 tree shaking
        path.resolve(__dirname, './src/*.js')
      ])
    })
  ],
  devServer: {
    hot: true,
    contentBase: path.join(__dirname, "./dist"),
    host: "0.0.0.0", // 可以使用手机访问
    port: 8080,
    historyApiFallback: true, // 该选项的作用所有的404都连接到index.html
    proxy: {
      // 代理到后端的服务地址，会拦截所有以api开头的请求地址
      "/api": "http://localhost:3000"
    }
  },
  resolve: {
    extension: ["", ".js", ".jsx", "ts", "tsx"], //指定extension,不用在require或是import的时候加文件扩展名,会依次尝试添加扩展名进行匹配
    alias: {  //配置别名,加快webpack查找模块的速度
      "@": path.join(__dirname, "src"),
      pages: path.join(__dirname, "src/pages"),
      router: path.join(__dirname, "src/router")
    }
  },
  optimization: {   //按需加载，提高首屏加载速度
    splitChunks: {  // 所有的 chunks 代码公共的部分分离出来成为一个单独的文件
      chunks: "all", 
    },
    usedExports:true, //清除代码中无用的js代码，只支持import方式引入，不支持commonjs的方式引入,mode是production生效，develpoment不生效
  },
}