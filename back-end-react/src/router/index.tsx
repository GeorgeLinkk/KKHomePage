import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { renderRoutes } from '../utils/renderRoutes'
import routes from './routes'

export class AppRouter extends React.Component {
    render() {
        return (<BrowserRouter>
        <Switch>
          {renderRoutes(routes)}
        </Switch>
      </BrowserRouter>)
    }
}

