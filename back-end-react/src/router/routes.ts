import { lazy } from "react"
import BasicLayout from "../layouts/BasicLayout"

const routes:Array<any> = [
  {
    path: '/',
    title: '系统中心',
    component: BasicLayout,
    indexRoute: '/home',
    childRoutes: [
      {
        path: '/home',
        component: lazy(() => import('../pages/Home')),
        requiresAuth: false,
      },
      {
        path: '/article',
        component: lazy(() => import('../pages/Article')),
        requiresAuth: false,
      }
    ],
    requiresAuth: false,
  },
  {
      path: '/login',
      component: lazy(() => import('../pages/Login')),
      requiresAuth: false,
  },
  {
      path: '*',
      component: lazy(() => import('../pages/NotFound')),
      requiresAuth: false,
  }
]
export default routes