import * as React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
interface routeObject{
  path: string,
  title: string,
  indexRoute?: string,
  childRoutes?: Array<routeObject>,
  requiresAuth?: boolean,
  [propName:string]:any
}
export const renderRoutes = (routes:routeObject[], authed?:boolean, authPath:string = '/login', extraProps:object = {}, switchProps:object = {}) => routes ? (
  <Switch {...switchProps}>
    {routes.map((route:routeObject, i) => (
      <Route
        key={route.key || i}
        path={route.path}
        exact={route.exact}
        strict={route.strict}
        render={(props:any) => {
          if (!route.requiresAuth || authed || route.path === authPath) {
            return <route.component {...props} {...extraProps} route={route} />
          }
          return <Redirect to={{ pathname: authPath, state: { from: props.location } }} />
        }}
      />
    ))}
  </Switch>
) : null
