import React, { Component } from 'react';

const initialState = {}
type State = Readonly<typeof initialState>
type Props = { match?:any,location?:any,route?:any}
class Home extends Component<Props, State> {
    render() { 
        return (  <h2>Home</h2> );
    }
}
 
export default Home;

