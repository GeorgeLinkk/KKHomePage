import React, { Component } from 'react';

const initialState = {}
type State = Readonly<typeof initialState>
type Props = { match?:any,location?:any,route?:any}
class Article extends Component<Props, State> {
    render() { 
        return (  <h2>Article</h2> );
    }
}
 
export default Article;
