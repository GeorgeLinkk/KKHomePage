import React, { Component } from 'react';

const initialState = {}
type State = Readonly<typeof initialState>
type Props = { match?:any,location?:any,route?:any}
class NotFound extends Component<Props, State> {
    render() { 
        return (  <h2>NotFound</h2> );
    }
}
 
export default NotFound;

