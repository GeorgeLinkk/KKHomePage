const Koa = require('koa');
const router = require('./routes/index');
const bodyParser = require('koa-bodyparser');
const checkToken = require('./utils/checkToken');

const app = new Koa();

app.use(checkToken)
app.use(bodyParser());

app.use(router.routes()).use(router.allowedMethods())

// 监听3000端口
app.listen(3000);