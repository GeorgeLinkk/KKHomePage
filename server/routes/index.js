
const Router = require('koa-router');
const  auth = require('../controllers/auth'); 
const  user = require('../controllers/user'); 
const  article = require('../controllers/article'); 

let fs = require('fs');
const router = new Router();

router
    .post('/auth/loginIn', auth.loginIn)
    .post('/auth/logout', auth.logout)
    .get('/article/getArticleList', article.getArticleList)
    .post('/article/addArticle', article.addArticle)
    .get('/user/getUserRouters', user.getUserRouters)
    

module.exports = router