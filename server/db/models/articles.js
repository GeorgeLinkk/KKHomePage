const { isValidObjectId } = require('mongoose')
const mongoose = require('../db')
const Schema = mongoose.Schema
const ArticlesSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    modifyDate:{
        type:Date,
        require:true
    },
    createDate:{
        type:Date,
        require:true
    },
    author: {
        type: String,
        required:true
    },
    authorid: {
      type: mongoose.Schema.Types.ObjectId,
      required:true
    },
    updaterid: {
      type: mongoose.Schema.Types.ObjectId,
      required:true
    },
    updater: {
      type: String,
      required:true
    },
    content:{
        type:String
    }
})
module.exports = mongoose.model('Articles', ArticlesSchema, 'Articles')