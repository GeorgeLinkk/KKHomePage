const mongoose = require('../db')
const Schema = mongoose.Schema
const TagsSchema = new Schema({
    title:{                 //标题
        type:String,
        required:true
    },
})
module.exports = mongoose.model('Tags', TagsSchema,'Tags')