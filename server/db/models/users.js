const mongoose = require('../db')
const Schema = mongoose.Schema
const UsersSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: false
  },
  roles: [{
    type: Schema.Types.ObjectId,
    ref:'Roles'
  }]
})
// 第三个参数记得加，否则无法跟Schema形成映射,返回的数据会为空数组
module.exports = mongoose.model('Users', UsersSchema,'Users')