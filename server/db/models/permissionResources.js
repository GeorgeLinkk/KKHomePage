const mongoose = require('../db')
const Schema = mongoose.Schema
const PermissionResourcesSchema = new Schema({
    url:{
        type:String,
        required:true
    },
    name:{
        type:String,
        require:true
    }
})
module.exports = mongoose.model('PermissionResources', PermissionResourcesSchema, 'PermissionResources')