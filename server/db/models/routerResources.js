const mongoose = require('../db')
const Schema = mongoose.Schema
const RouterResourcesSchema = new Schema({
    path:{
        type:String,
        required:true
    },
    name:{
        type:String,
        require:true
    }
})
module.exports = mongoose.model('RouterResources', RouterResourcesSchema, 'RouterResources')