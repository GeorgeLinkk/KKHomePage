const mongoose = require('../db')
const Schema = mongoose.Schema
const RolesSchema = new Schema({
    name:{
      type: String,
      required:true
    },
    routerResources:[{
      type: Schema.Types.ObjectId,
      ref:'RouterResources'
    }],
    permissionResources:[{
      type: Schema.Types.ObjectId,
      ref:'PermissionResources'
    }]
})
module.exports = mongoose.model('Roles', RolesSchema, 'Roles')