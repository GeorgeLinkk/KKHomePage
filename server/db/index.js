const Users = require('./models/users')
const Budgets = require('./models/budgets')
const Tags = require('./models/tags')
const Articles = require('./models/articles')
const RouterResources = require('./models/routerResources')
const PermissionResources = require('./models/permissionResources')
const Roles = require('./models/roles')

module.exports = {
    Users:Users,
    Budgets:Budgets,
    Tags:Tags,
    Articles:Articles,
    RouterResources:RouterResources,
    PermissionResources:PermissionResources,
    Roles:Roles
}