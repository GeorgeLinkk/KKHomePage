//对象数组去重
const uniqueObjArr = function(arr) {
  const res = new Map();
  return arr.filter((a) => !res.has(a) && res.set(a, 1))
}

module.exports = {
  uniqueObjArr
}