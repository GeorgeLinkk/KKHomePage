// 验证token
const jwt = require('jsonwebtoken')

// 检查 token
module.exports = async (ctx, next) => {
  //不需要校验token的接口路由
  const whiteList = ['loginIn','register','logout']
  const path = ctx.request.path;
  const isNeedCheck = whiteList.reduce((pre,next)=>{
    return (path.endsWith(next) | pre)
  },false)
  if (!isNeedCheck) {
  // 检验是否存在 token
  // axios.js 中设置了 authorization
    const token = ctx.get('X-Token')
    if (token === '') {
      ctx.response.status = 400;
      ctx.response.body = {
        code: -1,
        msg: 'nologin'
      }
    }
    // 检验 token 是否已过期
    try {
      await jwt.verify(token, 'linkk')
    } catch (err) {
      ctx.response.body = {
        code: -1,
        msg: 'expired'
      }
    }
    await next()
  }else{
    await next()
  }
}
