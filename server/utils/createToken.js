const jwt = require('jsonwebtoken')

module.exports = function (id) {
    const token = jwt.sign(
        {
            id: id
        },
        'linkk',
        {
            expiresIn: '2h'
        }
    )

    return token
}